/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/


//Modes for each alert
final int TYPE1 = 1;
final int TYPE2 = 2;


class Alert
{
  
  //------------ Private Variables -----------------
  
  private Arduino arduino;
  private boolean emulate;
  private int timeOn;
  private int timeOff;
  private int time;
  private boolean on;
  private boolean enabled;
  
  ArrayList<Port> ports = new ArrayList<Port> ();

  
  color c;
  int posx;
  int posy;
  int type; //type 1 or type2
  
  boolean isPWM;
  
  
  //------------ Private Methods -----------------
  private void draw()  
  {
      for(int i = 0; i < ports.size(); i++)
      {
        Port p = ports.get(i);
        p.draw();
        
      //  analogWrite(p.port_id,p.status);

      }
  
  
  }
     
  
  //------------ Protected Methods -----------------
  
  protected void startPattern(int type){}
  protected void stopPattern(){}
  protected void changeToON(){}
  protected void changeToOFF(){}
  protected void interpolate(float dt){}
  protected void setupPorts(){}  
 
  protected Port addPort(int x, int y,color c, int id, boolean pwm, int s)
  {
    Port p = new Port(x, y,c,id,pwm,s,this);
    ports.add(p);
    isPWM = pwm;
    
    if(!emulate)
    {
      if(isPWM) 
        arduino.pinMode(id,Arduino.SERVO);
      else
        arduino.pinMode(id,Arduino.OUTPUT);
    }
     return p;
    
  }
  
  
  protected void analogWrite(int port, int value)
  {
     if(!emulate)
     {
       if(isPWM)
       {
        arduino.analogWrite(port, value);
       }
       else
       {
         if(value == 255)
          arduino.digitalWrite(port,Arduino.HIGH);
         else 
          arduino.digitalWrite(port,Arduino.LOW);
       }
     }
        
  }
 
  protected void setCycleTime(int time_on, int time_off)
  {
     timeOn =time_on;
     timeOff = time_off;
    
  }
 

  
  //------------ Public Methods -----------------

  public Alert(boolean e, Arduino ard )
  {
     emulate = e;
     arduino = ard;
     on = false;
     enabled = false;

  }
  
  void update()
   {
      int elapsed =  (millis() - time);
 
      if(enabled)
      {
 
        if( on )
        {
          if(elapsed > timeOn) 
          {
             on= false;
            time = millis();
            changeToOFF();
          }
          interpolate((float) elapsed/timeOn);
            
        }
        else
        {
            if(elapsed > timeOff)
            {  
              on = true;
              time = millis();
              changeToON();
            }
            
            interpolate((float) elapsed/timeOff);
        }
      }
        
      draw();  
  
        
   }
 
  public void start(int t)
  {
       if(enabled == true)
         return;
       
       enabled = true;
       on = true;
       time = millis();       
    
       startPattern(t);
         
   
  }
  public void stop()
  {
       enabled = false;
       on = false;       
       stopPattern();
  }
 
  
}
