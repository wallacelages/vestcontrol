/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/

final int OUTPUT = 1;
final int PWM = 2;


class Port
{
  int status; //from 0 to 255
  int posx;
  int posy;
  color baseColor; //0->1 scale
  int port_id;
  boolean isPWM;
  int size;
  Alert alert;
  
  Port(int x, int y,color c, int id, boolean pwm, int s, Alert a)
  {
    posx = x;
    posy = y;
    status = 0;
    baseColor = c;
    port_id = id;
    isPWM = pwm;
    size = s;
    alert = a;
    
  }
  
  void setValue(int v)
  {
     status = v;
     alert.analogWrite(port_id,status);
  }
  
  void draw()
  {
    fill(red(baseColor)*status, green(baseColor)*status,blue(baseColor)*status);
    ellipse(posx,posy,size,size);   
    
  }
  
  
  
}
