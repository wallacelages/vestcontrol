/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/

//Actions for each alert
final int START = 1;
final int STOP = 0;

//Modality list and state
final int HAPTICS = 0;
final int VISUAL = 1;
final int AUDITORY = 2;



//Port definitions
final int ACKBUTTON = 52;

import processing.serial.*;
import cc.arduino.*;


class Vest
{
 
   //------------ Private Variables -----------------
     
   private int AlertMode = 0;
   private int AlertType = 1;
   int posx=270;
   int posy=20;
   Arduino arduino;
  //If true send signals to the board
   boolean Emulate;
   Alert alerts[] = new Alert[3];

   //------------ Private Methods -----------------
   
 
  
   //------------ Public Methods -----------------
     
   public Vest(processing.core.PApplet parent,boolean emulate)
   {
      Emulate = emulate;
      
      if(!Emulate)
      {
        println(Arduino.list()); 
        arduino = new Arduino(parent, "/dev/tty.AdafruitEZ-Link486b-SPP", 57600);
      }
      
      alerts[0] = new HapticsAlert(arduino,posx,posy,emulate);
      alerts[1] = new VisualAlert(arduino,posx,posy,emulate);
      alerts[2] = new AuditoryAlert(arduino,posx,posy,emulate);
      
      if(!emulate)
      arduino.pinMode(ACKBUTTON,Arduino.INPUT);
      
     
   } 
   
    public boolean CheckAck()
    {
      if(!Emulate)
        return (arduino.digitalRead(ACKBUTTON)==Arduino.HIGH?true:false);
      else
      {
         if(random(0,400)<2)
           return true; 
         else 
           return false;
      }
        
      
    }
   
     
    public void StartAlert()
    {
        alerts[AlertMode].start(AlertType);
    }
   
     public void StopAlert()
     {
        alerts[AlertMode].stop();
     }
 
     public void SetAlertMode(int mode)
     {
       AlertMode = mode;
        println("Current mode: " + AlertMode + " current type: " + AlertType);
  
     }
     
     public void SetAlertType(int type)
     {
       AlertType = type;
          println("Current mode: " + AlertMode + " current type: " + AlertType);
  
     }
     
     public void draw()
     {
         fill(140);
         
         rect(posx,posy,200,400);
         alerts[AlertMode].update();
     
     
       
     }
     
}
