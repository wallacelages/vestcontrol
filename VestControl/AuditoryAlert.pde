/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/

class AuditoryAlert extends Alert
{
  Port speaker1;
  Port speaker2;
  
  final int SPEAKER1 = 6;
  final int SPEAKER2 = 7;
  final int TIME_ON = 300;
  final int TIME_OFF = 300;
  final int MIN_VALUE = 0;
  final int MAX_VALUE = 128;
  
   AuditoryAlert(Arduino arduino, int x, int y, boolean emulate)
   {
      super(emulate,arduino);
      posy = y;
      posx = x;
      speaker1 = addPort(posx+27,posy+44,color(0,0,1),SPEAKER1,true,20);
      speaker1.setValue(0);

      speaker2 = addPort(posx+146,posy+44,color(0,0,1),SPEAKER2,true,20);
      speaker2.setValue(0);
      setCycleTime(TIME_ON,TIME_OFF);
      
      println("Turning off auditory alarm");
   }
     
         
    protected void startPattern(int t)
    {
        type = t;
      
        if(type == TYPE1)
        {
           speaker1.setValue(MAX_VALUE);
           speaker2.setValue(MAX_VALUE);
        }
    
    
         
         println("Starting auditory alert " + type);    
    }
  
    protected void stopPattern()
    {  
         speaker1.setValue(0);
         speaker2.setValue(0);
         
         print("Stoping auditory alert");   
     
    }
    protected void changeToON()
    {
   
       if(type == TYPE2)
       {
           speaker1.setValue(MAX_VALUE);
           speaker2.setValue(MAX_VALUE);
       }
    }
    
    protected void changeToOFF()
    {
       if(type == TYPE2)
       {
          speaker1.setValue (MIN_VALUE);
          speaker2.setValue(MIN_VALUE);
       }
 
    }
 
    protected void interpolate(float dt)
    {
   //    if(type == TYPE2)    
     //   {   
       //   speaker1.setValue((int)(MAX_VALUE*dt));
      //    print((int)(MAX_VALUE*dt) + " ");
      //  }
      
    }
 
  
}
