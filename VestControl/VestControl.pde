/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/

import java.io.*;

boolean EMULATE = false;
boolean running = false; //if the alert schedule is running
final int maxTime = 1; //max time before alert goes on (in seconds)
final int minTime = 0;  //min time before alert goes on (in seconds)
final int NUM_TASKS = 5; //number of tasks in the experiment

int taskCount  = 0;

int alertTime = 0; //time when the alarm shoud goes on (in ms)
int countDownTime = 0; //time when countown started (in ms)

Vest  vest;
Button [] buttons = new Button[6];
Button newButton;
Button runButton;


import javax.swing.*; 
String logFileName = "temp.txt";

final JFileChooser fc = new JFileChooser(); 
  
boolean newParticipant()
{
  int returnVal = fc.showSaveDialog(this);
  if (returnVal == JFileChooser.APPROVE_OPTION) { 
     File file = fc.getSelectedFile(); 
     logFileName = file.getAbsolutePath();
     String name = file.getName();
     
     taskCount = 0;
     
     println("Writing resuls to " + logFileName);
     
     logData("Participant ID");
     for(int i = 0; i < NUM_TASKS; i++)
     {
         logData(", Task " + i);       
     }
     logData("" +'\n');
   
     logData(name);
     
     return true;
  } 
  else { 
     return false;  
  }
  
}

void processKey(char k)
{
  switch(k)
  {
    //Suspend or start timer
    case 'r':
    case 'R':
          if(running == false) //schedule an alert
          {
               if(logFileName == null)
               {
                  runButton.reset(); 
                 newParticipant();
               }
               else
             {  
                 countDownTime = millis();
                 open("test");
                 alertTime = (int) random(minTime*1000,maxTime*1000);
                 println("Next alert in " + alertTime + " ms");
                 running = true;
             }
          }
          else //just stop the alert
          {
                vest.StopAlert();
                running = false;
                
                //write negative time to mean no Ack received
                logData(", -1");
                taskCount++;   
          }
  
          break;
          
    //Change to AUDITORY mode
    case 'a':
    case 'A':
          vest.SetAlertMode(AUDITORY);
          break;
          
   //Change to VISUAL mode
   case 'v':
   case 'V':
          vest.SetAlertMode(VISUAL);
          break;
 
   //Change to HAPTICS mode
   case 'h':
   case 'H':
          vest.SetAlertMode(HAPTICS);
          break;
          
   //Create a new Participant
   case 'n':
   case 'N':
          newParticipant();
          newButton.reset(); 
          break;
          
  //Change to type 1
   case '1':
          vest.SetAlertType(TYPE1);
          break;
  
  //Change to type 2
   case '2':
          vest.SetAlertType(TYPE2);
          break;
   }
}

//Process user input to start, change alerting mode and type
void keyReleased() {
  
  processKey(key);
  
}

void setup() {
  
  size(512  , 512);
  randomSeed(second());
  vest = new Vest(this,EMULATE);
    
  buttons[0] = new Button( 20, 20, 70, 50, "Haptics 1",  color(155,0,0) ); 
  buttons[1] = new Button( 150, 20, 70, 50, "Haptics 2",color(155,0,0) ); 
  buttons[2] = new Button( 20, 120, 70, 50, "Visual 1",color(0,155,0) ); 
  buttons[3] = new Button( 150, 120, 70, 50, "Visual 2",color(0,155,0) ); 
  buttons[4] = new Button( 20, 220, 70, 50, "Auditory 1",color(60,60,155) ); 
  buttons[5] = new Button( 150, 220, 70, 50, "Auditory 2",color(60,60,155) ); 
  
  runButton = new Button( 20, 320, 200, 50, "Run",color(155,155,15) ); 
  newButton = new Button( 20, 400, 200, 50, "New Participant",color(155,15,150) ); 
  

    
}

//Deactivate all buttons but one
void resetButtonsButOne(int one)
{

  for(int i=0; i < buttons.length; i++)
    if(i != one)
    {
      if(buttons[i] != null)
        buttons[i].reset();
    }
}

//Draw on screen Buttons
void drawButtons()
{
   for(int i=0; i < buttons.length; i++)
   {
      if(buttons[i] != null)
        buttons[i].draw();
   }
   
   runButton.draw();
   newButton.draw();
  
}


void mousePressed()
{
  
   //only activate the last clicked button
   for(int i =0; i< buttons.length; i++)
   {
     if(buttons[i] != null)
     if(buttons[i].activate(mouseX, mouseY))
     {
       resetButtonsButOne(i);
       String str = buttons[i].getLabel();
       processKey(str.charAt(0));
       processKey(str.charAt(str.length()-1));
     }
   }
   
   if(runButton.activate(mouseX,mouseY))
     processKey('r');
  
   if(newButton.activate(mouseX,mouseY))
     processKey('n');
   
}

void logData(String str)
{
    PrintWriter pw = null;
    try
    { 
        pw = new PrintWriter(new FileWriter(logFileName, true));
        pw.print(str);
    } 
    catch (IOException e) 
    { 
        e.printStackTrace(); // Dump and primitive exception handling...
    }
    finally
    {
        if (pw != null)
        {
          pw.close();
        }
   }
  
}

void draw() {
  
  background(200);
  if(running)
  {
    //Check if it is time to alert
    if((millis()-countDownTime) >= alertTime)
    {
        vest.StartAlert();
    
      //if user pressed the ack button
     if(vest.CheckAck())
      {
         int ack_time = millis()-(countDownTime+alertTime);
         println("ack in " + ack_time + " ms");    
         running = false;      
         runButton.reset(); 
         vest.StopAlert();
         
         logData(" , "+ack_time);
         taskCount++;        
      }
      
    }
   // else  print(alertTime - elapsedTime + " ");
  
         
  }
    
    
    
    textSize(12);
    textAlign(LEFT, CENTER);
    fill(0);  
    
    if(running)
    {
         text("Running Task " + taskCount,10,480); 
      
    }
    
    
    if(logFileName != null)
      text("Writing to: " + logFileName, 10, 500 ); 
    else
      text("Create a new participant!", 10, 500 ); 
 
    drawButtons();
    vest.draw();
    
   // println(" (" + (mouseX - 270) + " " + (mouseY -20) + " )");
}
