/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/


class HapticsAlert extends Alert
{
  Port [] motor1 = new Port[2]; //motors on the neck
  Port [] motor2 = new Port[4]; //motors on the front
  
  //Neck motors
  final int MOTORNECK1 = 4;
  final int MOTORNECK2 = 5;
  
  //front motors
  final int MOTORFRONTBEGIN = 10;
  final int NUM_MOTORS_FRONT = 4;

  
  final int TIME_ON = 300;
  final int TIME_OFF = 200;
  final int MIN_VALUE = 0;
  final int MAX_VALUE = 255;
  
   HapticsAlert(Arduino arduino, int x, int y, boolean emulate)
   {
      super(emulate,arduino);
      posy = y;
      posx = x;
      
      //motors on the neck
      motor1[0] = addPort(posx+75,posy+80,color(1,0,0),MOTORNECK1,true,10);
      motor1[0].setValue(0);

      motor1[1] = addPort(posx+105,posy+80,color(1,0,0),MOTORNECK2,true,10);
      motor1[1].setValue(0);

      //motors on the front
      for(int i=0; i<NUM_MOTORS_FRONT;i++)
      {
        motor2[i] = addPort(posx+95,posy+130+i*30,color(1,0,0),MOTORFRONTBEGIN+i,true,10);
        motor2[i].setValue(0);         
      }



      setCycleTime(TIME_ON,TIME_OFF);
   }
     
         
    protected void startPattern(int t)
    {
        type = t;
      
        if(type == TYPE1)
        {
           motor1[0].setValue(MIN_VALUE);
           motor1[1].setValue(MIN_VALUE);
        }  
        else   
        {
          for(int i=0; i<NUM_MOTORS_FRONT;i++) 
            motor2[i].setValue(MIN_VALUE);
        }
         println("Starting haptics alert " + type);    
    }
  
    protected void stopPattern()
    {  
        //stop motors on the neck
         motor1[0].setValue(0);
         motor1[1].setValue(0);
         
         for(int i=0; i<NUM_MOTORS_FRONT;i++) 
           motor2[i].setValue(0);
         
         print("Stoping haptics alert");   
     
    }
    protected void changeToON()
    {
   
       if(type == TYPE1)
       {
           motor1[0].setValue(MAX_VALUE);
           motor1[1].setValue(MAX_VALUE);
       }
       else
           for(int i=0; i<NUM_MOTORS_FRONT;i++)
             motor2[i].setValue(MAX_VALUE);
    }
    
    protected void changeToOFF()
    {
      motor1[0].setValue(0);
      motor1[1].setValue(0);
      
      for(int i=0; i<NUM_MOTORS_FRONT;i++) 
        motor2[i].setValue(0);
 
    }
 
 
  
}
