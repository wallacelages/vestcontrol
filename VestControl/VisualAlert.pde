/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/


class VisualAlert extends Alert
{
  ArrayList<Port> LedsLeft = new ArrayList<Port> ();
  ArrayList<Port> LedsRight = new ArrayList<Port> ();
  
  
  int [] left_mapping = new int[12];
  int [] right_mapping = new int[12];

  final int TIME_ON = 300;
  final int TIME_OFF = 200;
  final int MIN_VALUE = 0;
  final int MAX_VALUE = 255;
  
 
  
  
   VisualAlert(Arduino arduino, int px, int py, boolean emulate)
   {
      super(emulate,arduino);
      posy = py;
      posx = px;
      setCycleTime(TIME_ON,TIME_OFF);
      
     
      left_mapping[0] = 47;
  left_mapping[1] = 46;
  left_mapping[2] = 42;
  left_mapping[3] = 43;
  left_mapping[4] = 49;
  left_mapping[5] = 48;
  left_mapping[6] = 40;
  left_mapping[7] = 41;
  left_mapping[8] = 51;
  left_mapping[9] = 50;
  left_mapping[10] = 38;
  left_mapping[11] = 39;
  
  right_mapping[0] = 26;
  right_mapping[1] = 27;
  right_mapping[2] = 35;
  right_mapping[3] = 34;
  right_mapping[4] = 24;
  right_mapping[5] = 25;
  right_mapping[6] = 33;
  right_mapping[7] = 32;
  right_mapping[8] = 22;
  right_mapping[9] = 23;
  right_mapping[10] = 31;
  right_mapping[11] = 30;
     
      int index = 0;
      for(int y = 0; y < 3; y++)
        for(int x = 0; x < 4; x++)
        {
            Port led = addPort(posx+20+x*20,posy+44+y*25,color(0,1,0),left_mapping[index],false,10);
            led.setValue(0);
            LedsLeft.add(led);
 
            led = addPort(posx+130+x*20,posy+44+y*25,color(0,1,0),right_mapping[index],false,10);
            led.setValue(0);
            LedsRight.add(led);
            
            index++;
      //      println(left_mapping[index]);
          
        }
            
            
   }
     
         
    protected void startPattern(int t)
    {
        type = t;
      
        if(type == TYPE1)
        {
            for(int i = 0; i< LedsRight.size(); i++)
            {
               LedsRight.get(i).setValue(MIN_VALUE);
               LedsLeft.get(i).setValue(MIN_VALUE);
            }
        }  
        else   
        {
            for(int i = 0; i< LedsRight.size(); i++)
            {
               LedsRight.get(i).setValue(MIN_VALUE);
               LedsLeft.get(i).setValue(MIN_VALUE);
            }
        }  
         
       println("Starting visual alert " + type);    
    }
  
    protected void stopPattern()
    {  
            for(int i = 0; i< LedsRight.size(); i++)
            {
               LedsRight.get(i).setValue(0);
               LedsLeft.get(i).setValue(0);
            }
    
         
         print("Stoping visual alert");   
     
    }
    protected void changeToON()
    {
   
       if(type == TYPE1)
          for(int i = 0; i< LedsRight.size(); i++)
            {
               LedsRight.get(i).setValue(MAX_VALUE);
               LedsLeft.get(i).setValue(MAX_VALUE);
            }    
   
    }
    
    protected void changeToOFF()
    {
         if(type == TYPE1)
            for(int i = 0; i< LedsRight.size(); i++)
            {
               LedsRight.get(i).setValue(0);
               LedsLeft.get(i).setValue(0);
            }
 
    }
    
    protected void interpolate(float dt)
    {
       //  print(dt);
 
      if(type == 2)
      {
       if(dt < 0.33)
       {
           
            for(int i = 0; i < LedsRight.size(); i++)
               if( (i >= 8) && (i < 12))
               {
                 LedsRight.get(i).setValue(MAX_VALUE);
                 LedsLeft.get(i).setValue(MAX_VALUE);
               }
               else
               {
                 LedsRight.get(i).setValue(MIN_VALUE);
                 LedsLeft.get(i).setValue(MIN_VALUE);
               }
               
       }
       else if(dt < 0.66)
       {
           for(int i = 0; i < LedsRight.size(); i++)
             if( (i >= 4) && (i < 8))
               {
                 LedsRight.get(i).setValue(MAX_VALUE);
                 LedsLeft.get(i).setValue(MAX_VALUE);
               }
               else
               {
                 LedsRight.get(i).setValue(MIN_VALUE);
                 LedsLeft.get(i).setValue(MIN_VALUE);
               }
               
          
       }
       
       else 
       {
           for(int i = 0; i < LedsRight.size(); i++)
             if( (i >= 0) && (i < 4))
               {
                 LedsRight.get(i).setValue(MAX_VALUE);
                 LedsLeft.get(i).setValue(MAX_VALUE);
               }
               else
               {
                 LedsRight.get(i).setValue(MIN_VALUE);
                 LedsLeft.get(i).setValue(MIN_VALUE);
               }
               
        
       }
         
      }
 
      
      
    }
 
 
  
}
