/*
*      Alert vest experiment control/log
*      Wearable and Ubiquitous computing class / spring 2015
*      Wallace Lages
*/

public class Button
{
    float x,y,width,height;
    boolean on;
    String label;
    color back_color;

    Button ( float xx, float yy, float ww, float hh, String text, color c) 
    {
        x = xx; y = yy; width = ww; height = hh;
        label = text;
        back_color = c;
    }

    void reset()
    {
      on = false;
    }

    String getLabel()
    {
      return label;
    }

    boolean activate (int px, int py)
    {
        // Test it the button has been pressed
        if(( px >= x) && (px < x+width) && (py >y) && (py < y+height))
        {
          on = !on;
          return true;
        }
        else return false;
    }

    void draw ()
    {
      
        fill( on ? back_color : 140 );
        rect( x, y, width, height );
        textSize(12);
        textAlign(CENTER, CENTER);
        fill(0);  
        text(label, (x+width/2), (y+height/2)); 
    }
}
